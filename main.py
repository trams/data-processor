#!/usr/bin/env python
import time
from datetime import datetime
import psycopg2
import requests
from flask import Flask

from NotificationHub import NotificationHub

app = Flask(__name__)

# CONSTANTS
API_KEY = '13877bf6-79a1-498a-8ec8-76f7be62493a'
TRAM_ID = 'c7238cfe-8b1f-4c38-bb4a-de386db7e776'
BUS_ID = 'f2e5503e-927d-4ad3-9500-4ab9e55deb59'
TRAM_URL = 'https://api.um.warszawa.pl/api/action/wsstore_get/'
BUS_URL = 'https://api.um.warszawa.pl/api/action/busestrams_get/'
DELAY = 10.0
BUS_TYPE = 1
TRAM_ID_NAME = "id"
BUS_ID_NAME = "resource_id"

# CONSTANTS
DB_NAME = "history"
PORT = 5432
USER = "ogj29ch@tramwaje"
PASSWORD = "@/m+Xu!f"
HOST = "tramwaje.postgres.database.azure.com"
RUNNING_STRING = "RUNNING"
TRAM_HEADER = ["Running", "FirstLine", "Lon", "Lines", "Time", "Lat", "LowFloor", "Brigade"]
BUS_HEADER = ["Lat", "Lines", "Brigade", "Lon", "Time"]

# NOTIFICATION_HUB
IS_DEBUG_MODE = False;


@app.route('/')
def hello_world():
    conn = psycopg2.connect(dbname=DB_NAME, port=PORT, user=USER, password=PASSWORD, host=HOST)
    cur = conn.cursor()
    cur.execute("SELECT time FROM trams ORDER BY time desc limit 1;")
    last_time_tram = cur.fetchall()[0]
    cur.execute("SELECT time FROM buses ORDER BY time desc limit 1;")
    last_time_buses = cur.fetchall()[0]
    conn.commit()
    cur.close()
    conn.close()
    return "Expected delay: %s | Last tram import: %s | Last buses import: %s" % (
        DELAY, last_time_tram[0], last_time_buses[0])


@app.route('/run')
def run():
    starttime = time.time()
    while True:
        gettram(TRAM_URL, TRAM_ID_NAME, TRAM_ID, API_KEY)
        getbus(BUS_URL, BUS_ID_NAME, BUS_ID, API_KEY, BUS_TYPE)
        time.sleep(DELAY - ((time.time() - starttime) % DELAY))


def gettram(resource_url, id_name, resource_id, api_key, type=None):
    start_time = time.time()
    start_time_date = datetime.now()
    r = requests.get(resource_url, params={id_name: resource_id, 'apikey': api_key, 'type': type})
    if r.status_code == requests.codes.ok:
        items = r.json()
        try:
            errors = []
            conn = psycopg2.connect(dbname=DB_NAME, port=PORT, user=USER, password=PASSWORD, host=HOST)
            cur = conn.cursor()
            for i in items['result']:
                try:
                    clean = {}
                    for k, v in i.items():
                        if isinstance(v, str):
                            clean[k] = v.strip()
                        else:
                            clean[k] = v
                    point_string = "POINT(%s %s)" % (clean["Lon"], clean["Lat"])
                    cur.execute(
                        "INSERT INTO trams_l(running, firstLine, lines, time, lowFloor, brigade, geom,chunktime) VALUES (%s, %s, %s, %s, %s, %s, ST_GeomFromText(%s, 4326),%s)",
                        [clean["Status"] == RUNNING_STRING, clean["FirstLine"], clean["Lines"], clean["Time"],
                         clean["LowFloor"], clean["Brigade"], point_string, start_time_date])
                except AttributeError as inst:
                    print(inst)
                    errors.append("{0}".format(inst))
            elapsed_time = time.time() - start_time
            cur.execute("INSERT INTO log_l(collection, time,chunktime) VALUES (%s, %s,%s)",
                        ["Trams", elapsed_time, start_time_date])
            if len(errors) > 0:
                manage_error(cur,start_time_date,"Trams","AttributeError",errors)
            conn.commit()
            cur.close()
            conn.close()
            print(
                "TRAMS COLLECTION [TIME %s | TOTAL %s]" % (start_time_date.strftime('%Y_%m_%d-%H_%M_%S'), elapsed_time))
        except psycopg2.Error as exc:
            print(exc)
            notify(str(exc))


def notify(message):
    hub = NotificationHub(
        "Endpoint=sb://generalnotificationhubnamespace.servicebus.windows.net/;SharedAccessKeyName=DefaultFullSharedAccessSignature;SharedAccessKey=9zmd6FMV2rEb/XW1JcRs5fMqWnwuxVUdfo6dicKhPV0=",
        "GeneralNotificationHub", IS_DEBUG_MODE)
    wns_payload = "<toast><visual><binding template=\"ToastText01\"><text id=\"1\">" + message + "</text></binding></visual></toast>"
    hub.send_windows_notification(wns_payload)


def getbus(resource_url, id_name, resource_id, api_key, type=None):
    start_time = time.time()
    start_time_date = datetime.now()
    r = requests.get(resource_url, params={id_name: resource_id, 'apikey': api_key, 'type': type})
    if r.status_code == requests.codes.ok:
        items = r.json()
        try:
            errors = []
            conn = psycopg2.connect(dbname=DB_NAME, port=PORT, user=USER, password=PASSWORD, host=HOST)
            cur = conn.cursor()
            for i in items['result']:
                try:
                    clean = {}
                    for k, v in i.items():
                        if isinstance(v, str):
                            clean[k] = v.strip()
                        else:
                            clean[k] = v
                    point_string = "POINT(%s %s)" % (clean["Lon"], clean["Lat"])
                    cur.execute(
                        "INSERT INTO buses_l(lines, time, brigade, geom,chunktime) VALUES (%s, TIMESTAMP %s, %s, ST_GeomFromText(%s, 4326),%s)",
                        [clean["Lines"], clean["Time"], clean["Brigade"], point_string, start_time_date])
                except AttributeError as inst:
                    print(inst)
                    errors.append("{0}".format(inst))
            elapsed_time = time.time() - start_time
            cur.execute("INSERT INTO log_l(collection, time,chunktime) VALUES (%s, %s, %s)",
                        ["Buses", elapsed_time, start_time_date])

            if len(errors) > 0:
                manage_error(cur,start_time_date,"Buses","AttributeError",errors)
            conn.commit()
            cur.close()
            conn.close()

            print(
                "BUSES COLLECTION [TIME %s | TOTAL %s]" % (start_time_date.strftime('%Y_%m_%d-%H_%M_%S'), elapsed_time))
        except psycopg2.Error as exc:
            notify(str(exc))
            print(exc)


def manage_error(cur, time, collection, type, errors):
    for r in errors:
        cur.execute(
            "INSERT INTO errors_l(collection, chunktime, error, type) VALUES  (%s, %s, %s,%s)",
            [collection, time, r,type])
    notify(str(len(errors)) + " "+type+"s at "+collection)
    print(str(len(errors)) + " "+type+"s at "+collection)


if __name__ == '__main__':
    app.run()
